﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : Singleton<GameManager>
{

    public int Score;

    private void Start()
    {
        Player.Instance.enabled = false;
    }

    public void GameStart()
    {
        EventManager.GameStart();
        Target.Instance.Health = 100;
        Player.Instance.enabled = true;
        Score = 0;
        UIManager.Instance.UpdateUI();
    }

    public void GameOver()
    {
        EventManager.GameEnd();
        Player.Instance.enabled = false;
    }


    public void UpdateScore(int Amount)
    {
        Score += Amount;
        UIManager.Instance.AnimateObject(UIManager.Instance.GameScoreText.transform);
        UIManager.Instance.UpdateUI();
    }

    public void Quit()
    {
        Application.Quit();
    }
}
