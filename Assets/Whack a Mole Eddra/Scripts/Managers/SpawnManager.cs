﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : Singleton<SpawnManager>
{
    public List<SpawnArea> spawnAreas;

    public float SpawnRate;

    public List<EnemyAI> Enemies = new List<EnemyAI>();

    private void Start()
    {
        EventManager.OnGameStart += SpawnObjects;
        EventManager.OnGameEnd += GameOver;
    }


    public void GameOver()
    {
        StopAllCoroutines();
    }

    public void SpawnObjects()
    {
        StartCoroutine(SpawnObjectsCo());
    }

    IEnumerator SpawnObjectsCo()
    {
        while(true)
        {
            yield return new WaitForSeconds(SpawnRate);
            spawnAreas[Random.Range(0, spawnAreas.Count)].SpawnObject();
            if (SpawnRate > 2f)
                SpawnRate = SpawnRate - ((SpawnRate * 10) / 100);
        }
    }
}
