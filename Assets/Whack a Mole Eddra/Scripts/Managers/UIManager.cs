﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIManager : Singleton<UIManager>
{
    [Header("Panels")]
    public Panel MainMenuPanel;
    public Panel GamePanel;


    [Header("UI Components")]
    public Slider HealthBar;
    public Text MainMenuScoreText;
    public Text GameScoreText;

    private void Start()
    {
        EventManager.OnGameStart += GameStart;
        EventManager.OnGameEnd += GameOver;
    }


    void GameStart()
    {
        MainMenuPanel.HidePanel();
        GamePanel.ShowPanel();
        UpdateUI();
    }

    void GameOver()
    {
        MainMenuPanel.ShowPanel();
        GamePanel.HidePanel();
    }

    public void UpdateUI()
    {
        HealthBar.value = Target.Instance.Health;
        GameScoreText.text = "Score: " + GameManager.Instance.Score.ToString();
        //GameScoreText.transform.DOPunchScale(Vector3.one * 2, 1f);
        MainMenuScoreText.text = "Score: \n" + GameManager.Instance.Score;
    }

    public void AnimateObject(Transform ObjectToAnimate)
    {
        if(ObjectToAnimate.localScale == Vector3.one)
        {
            ObjectToAnimate.DOPunchScale(Vector3.one * 0.8f, 0.5f);
        }
            
    }
}
