﻿using UnityEngine.Events;

public class EventManager
{
    public delegate void GameStartAction();
    public static GameStartAction OnGameStart;

    public delegate void GameEndAction();
    public static GameEndAction OnGameEnd;


    public static void GameStart()
    {
        if (OnGameStart != null)
            OnGameStart.Invoke();
    }

    public static void GameEnd()
    {
        if (OnGameEnd != null)
            OnGameEnd.Invoke();
    }
}
