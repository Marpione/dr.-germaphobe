﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnArea : MonoBehaviour
{
    public float Redius;
    public string SpawnerPrefabName;
    public List<string> EnemyPrefabName;
    public string BloodCellPrefabName;


    public void SpawnObject()
    {
        int chance = Random.Range(0, 10);
        Vector3 spawnPos = transform.position + Random.insideUnitSphere * Redius;
        spawnPos.y = transform.position.y;
        GameObject go = PoolingSystem.Instance.InstantiateAPS(SpawnerPrefabName, spawnPos, Quaternion.identity);
        Spawner spawner = go.GetComponent<Spawner>();
        if (chance <= 7)
        {
            spawner.CreateObject(EnemyPrefabName[Random.Range(0, EnemyPrefabName.Count)]);
        }
        else
            spawner.CreateObject(BloodCellPrefabName);

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, Redius);
    }
}
