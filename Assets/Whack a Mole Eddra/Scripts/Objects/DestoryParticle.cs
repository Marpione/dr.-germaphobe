﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestoryParticle : MonoBehaviour
{
    ParticleSystem particleSystem;
    ParticleSystem ParticleSystem { get { return (particleSystem == null) ? particleSystem = GetComponent<ParticleSystem>() : particleSystem; } }
    // Update is called once per frame
    void Update()
    {
        if (!ParticleSystem.isPlaying)
            PoolingSystem.DestroyAPS(gameObject);
    }
}
