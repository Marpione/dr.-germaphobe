﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Spawner : MonoBehaviour
{
    public string VFXName;
    public Transform Graphic;
    AudioSource audioSource;
    AudioSource AudioSource { get { return (audioSource == null) ? audioSource = GetComponent<AudioSource>() : audioSource; } }

    public AudioClip ExplosionSound;

    private void OnEnable()
    {
        Graphic.gameObject.SetActive(true);
    }

    public void CreateObject(string PrefabName)
    {
        StartCoroutine(CreateObjectCo(PrefabName));
    }

    IEnumerator CreateObjectCo(string PrefabName)
    {
        transform.localScale = Vector3.one;
        transform.DOScale(15, 2f);
        yield return new WaitForSeconds(1.8f);
        Graphic.gameObject.SetActive(false);
        GameObject go = PoolingSystem.Instance.InstantiateAPS(VFXName, transform.position, Quaternion.identity);
        ParticleSystem particleSystem = go.GetComponent<ParticleSystem>();
        particleSystem.Play();
        AudioSource.PlayOneShot(ExplosionSound);
        PoolingSystem.Instance.InstantiateAPS(PrefabName, transform.position, Quaternion.identity);

        while (AudioSource.isPlaying)
            yield return null;

        PoolingSystem.DestroyAPS(gameObject);
    }
}
