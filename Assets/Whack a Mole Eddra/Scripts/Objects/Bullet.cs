﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bullet : MonoBehaviour
{
    public float ExplotionRange;
    public string VFXPrefabName;
    public Transform Graphic;
    public AudioClip explotionSound;

    Rigidbody rigidbody;
    Rigidbody Rigidbody { get { return (rigidbody == null) ? rigidbody = GetComponent<Rigidbody>() : rigidbody; } }

    Collider collider;
    Collider Collider { get { return (collider == null) ? collider = GetComponent<Collider>() : collider; } }
    
    AudioSource audioSource;
    AudioSource AudioSource { get { return (audioSource == null) ? audioSource = GetComponent<AudioSource>() : audioSource; } }

    private void OnEnable()
    {
        Graphic.gameObject.SetActive(true);
        Collider.enabled = true;
    }

    public void Launch(Vector3 Destination)
    {
        transform.DOMove(Destination, 2f);
        Rigidbody.AddTorque((Random.insideUnitSphere * 50) * 50);
    }

    private void OnCollisionEnter(Collision collision)
    {
        foreach (var enemy in SpawnManager.Instance.Enemies)
        {
            if(enemy.gameObject.activeInHierarchy)
            {
                if (Vector3.Distance(enemy.transform.position, transform.position) < ExplotionRange)
                {
                    enemy.Kill();
                    GameManager.Instance.UpdateScore(enemy.Point);
                    GameObject floatingTextPrefab = PoolingSystem.Instance.InstantiateAPS("FloatingText", transform.position, Quaternion.identity);
                    FloatingText floatingText = floatingTextPrefab.GetComponent<FloatingText>();
                    if (floatingText)
                        floatingText.Animate(enemy.Point);
                }
            }
        }
        Camera.main.transform.DOShakeRotation(0.5f, 15, 15, 90);
        GameObject go = PoolingSystem.Instance.InstantiateAPS(VFXPrefabName, transform.position, Quaternion.identity);
        ParticleSystem particleSystem = go.GetComponent<ParticleSystem>();
        particleSystem.Play();

        Kill();
    }

    public void Kill()
    {
        StartCoroutine(KillCo());
    }

    IEnumerator KillCo()
    {
        AudioSource.PlayOneShot(explotionSound);
        Graphic.gameObject.SetActive(false);
        Collider.enabled = false;
        while (AudioSource.isPlaying)
            yield return null;

        PoolingSystem.DestroyAPS(gameObject);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, ExplotionRange);
    }
}
