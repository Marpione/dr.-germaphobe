﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FloatingText : MonoBehaviour
{

    public Text text;

    private void Start()
    {
        Animate(3);
    }

    public void Animate(int value)
    {
        transform.LookAt(Camera.main.transform);
        text.color = Color.white;
        transform.localScale = Vector3.zero;
        Sequence scaleSequence = DOTween.Sequence();
        scaleSequence.Append(transform.DOScale(Vector3.one * 1, 0.1f));
        scaleSequence.Append(transform.DOMoveY(1, 0.5f));
        scaleSequence.Insert(0, transform.DOShakePosition(2, 1, 25));
        scaleSequence.Append(transform.DOScale(Vector3.zero, 0.1f));
        scaleSequence.OnComplete(KillObject);

        //transform.DOShakePosition(0.5f);
        //text.DOFade(0, 1.5f);
        //transform.DOMove(Camera.main.transform.position, 2f).OnComplete(KillObject);


        List<Vector3> path = new List<Vector3>();

        

        if(value > 0)
        {
            text.text = "+ " + value;
            text.color = Color.green;
        }
        else
        {
            text.text = value.ToString();
            text.color = Color.red;
        } 
    }

    void KillObject()
    {
        PoolingSystem.DestroyAPS(gameObject);
    }

    private void OnDisable()
    {
        transform.DOKill();
    }
}
