﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Target : Singleton<Target>
{
    public float Health;

    public void ReciveDamage(float Damage)
    {
        Health -= Damage;
        UIManager.Instance.AnimateObject(UIManager.Instance.HealthBar.transform);
        UIManager.Instance.UpdateUI();


        if (Health <= 0)
            GameManager.Instance.GameOver();
    }
}
