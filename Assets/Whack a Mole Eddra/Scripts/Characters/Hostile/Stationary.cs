﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Stationary : EnemyAI
{
   
    private void OnEnable()
    {
        Attack();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    void Attack()
    {
        StartCoroutine(AttackCo());
    }

    IEnumerator AttackCo()
    {
        Graphic.gameObject.SetActive(true);
        transform.DOLookAt(Target.Instance.transform.position, turnSpeed, AxisConstraint.Y);

        while(true)
        {
            yield return new WaitForSeconds(FireRate);
            GameObject go = PoolingSystem.Instance.InstantiateAPS(ShootingVFXPrefabName, firePoint.position, firePoint.rotation);
            ParticleSystem particleSystem = go.GetComponent<ParticleSystem>();

            if(!AudioSource.isPlaying)
                AudioSource.PlayOneShot(shootingSound);

            particleSystem.Play();
            Target.Instance.ReciveDamage(Damage);
        }
    }
}
