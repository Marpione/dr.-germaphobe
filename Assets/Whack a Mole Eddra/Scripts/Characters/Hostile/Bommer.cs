﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bommer : EnemyAI
{
    private void OnEnable()
    {
        Attack();
    }

    private void OnDisable()
    {
        transform.DOKill();
        StopAllCoroutines();
    }

    public void Attack()
    {
        StartCoroutine(AttackCo());
    }

    IEnumerator AttackCo()
    {
        Graphic.gameObject.SetActive(true);
        transform.DOLookAt(Target.Instance.transform.position, turnSpeed, AxisConstraint.Y);
        transform.DOMove(Target.Instance.transform.position, MoveSpeed);

        while (Vector3.Distance(transform.position, Target.Instance.transform.position) > 1f)
        {
            if(!gameObject.activeInHierarchy)
                DOTween.Kill(gameObject);

            yield return null;
        }

        GameObject go = PoolingSystem.Instance.InstantiateAPS(ShootingVFXPrefabName, firePoint.transform.position, Quaternion.identity);
        ParticleSystem particleSystem = go.GetComponent<ParticleSystem>();

        if (!AudioSource.isPlaying)
            AudioSource.PlayOneShot(shootingSound);

        particleSystem.Play();
        Target.Instance.ReciveDamage(Damage);
        transform.DOKill();
        Kill();
    }
}
