﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyAI : MonoBehaviour
{
    public Transform firePoint;
    public Transform Graphic;
    public float MoveSpeed;
    public float turnSpeed;
    public float FireRate;
    public float Damage;
    public int Point;
    public string ShootingVFXPrefabName;

    public AudioClip shootingSound;
    public AudioClip deathSound;

    AudioSource audioSource;
    protected AudioSource AudioSource { get { return (audioSource == null) ? audioSource = GetComponent<AudioSource>() : audioSource; } }


    private void Start()
    {
        EventManager.OnGameEnd += GameOver;
        SpawnManager.Instance.Enemies.Add(this);
    }

    private void OnEnable()
    {
        Graphic.gameObject.SetActive(true);
    }

    void GameOver()
    {
        Kill();
    }

    public void Kill()
    {
        if(gameObject.activeInHierarchy)
            StartCoroutine(KillCo());
    }

    IEnumerator KillCo()
    {
        
        if (!AudioSource.isPlaying)
            AudioSource.PlayOneShot(deathSound);

        Graphic.gameObject.SetActive(false);
        while (AudioSource.isPlaying)
            yield return null;

        transform.DOKill();
        PoolingSystem.DestroyAPS(gameObject);

    }
}
