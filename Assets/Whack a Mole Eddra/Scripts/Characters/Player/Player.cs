﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : Singleton<Player>
{
    public Transform firePoint;
    public string bulletPrefabName;

    Animator animator;
    Animator Animator { get { return (animator == null) ? animator = GetComponent<Animator>() : animator; } }

    RaycastHit hit;

    private void Start()
    {
        transform.DOLookAt(Target.Instance.transform.position, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if(Input.GetButtonDown("Fire1"))
        {
            if (Animator.GetCurrentAnimatorStateInfo(0).IsName("idle"))
            {
                if (Physics.Raycast(ray, out hit))
                {
                    Animator.SetFloat("fireSpeed", 0.5f);
                    Animator.SetTrigger("fire");
                    transform.DOLookAt(hit.point, 1f).OnComplete(ResetRotation);
                }
            } 
        }
    }

    void ResetRotation()
    {
        transform.DOLookAt(Target.Instance.transform.position, 1f);
    }

    public void Fire()
    {
        GameObject go = PoolingSystem.Instance.InstantiateAPS(bulletPrefabName, firePoint.position, Quaternion.identity);
        Bullet bullet = go.GetComponent<Bullet>();

        if (bullet)
            bullet.Launch(hit.point);

        
        //Vector3 dir = hit.point - go.transform.position;
    }
}
